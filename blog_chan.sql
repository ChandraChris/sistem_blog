-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 16, 2018 at 03:48 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog_chan`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `isi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `isi`) VALUES
(1, 'Nama saya adalah Christian Chandra. Bisa dipangil C Chan. 2 Tahun saya sudah di bidang pemrograman. 2 tahun saya di web devolopment dan android devolopment. Saya sih lebih tertarik dengan Web Devolopment');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(255) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `penulis` varchar(100) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `judul`, `isi`, `penulis`, `tanggal`) VALUES
(1, 'Belajar HTML 5 yang SERU!! Dan MUDAH!!', 'html sangat tidak asing lagi bagi programmer. Karena dari sinilah kebanyakan programmer memulai pembelajarannya. Jadi jika ingin menguasai web devolopment, pastikan menguasai html dulu ya gan!!', 'C Chan', '2018-10-03'),
(2, 'Lanjut belajar CSS!!!', 'Disini sangat lah penting bagi designer web, karena css ini merupakan bahasa sehari hari dalam kehidupannya.\r\nCMIIW', 'Jiamik', '2018-10-12'),
(3, 'Javascript Jos Gandos!!', 'Selanjutnya kita belajar javascript gan. Javascript adalah bahasa pemrograman yang paling friendly dengan bahasa manusia namun banyak orang yang belum bisa menguasai sepenuhnya!!', 'Andreww', '2018-10-16'),
(4, 'Belajar Jquery', 'Jadi Jquery ini adalah salah satu library nya javascript gan. Jquery ini sangat berperan penting dalam suatu web. Jquery ini berperan bagian animasi yang sangat epic seperti slider show dll.', 'ADJI', '2018-10-26'),
(5, 'Gabut Parah', 'Gabut adalah kondisi dimana kita sedang tidak melakukan apa apa.', 'Didan', '2018-10-17'),
(6, 'Disini', 'Disini adalah kata kata yang menunjukkan letak dimana ia berada sekarang', 'Someone', '2018-10-27'),
(7, 'dani', 'dani adalah nama seseorang laki laki', 'Dani', '2018-11-10'),
(8, 'Arena Of Valor (AOV)', 'Arena Of Valor adalah game moba yang bersaing dengan mobile legend, VainGlory, dll. Game ini menghadirkan karakter marvel yang sangat menarik dan desain graphicnya ga usah ditanya lagi coy', 'Fans AOV', '2018-10-30'),
(9, 'Kamui', 'Kamui adalah salah satu jenis mata sharingan yang berada di anime Naruto.', 'Fans Uchiha', '2018-10-14');

-- --------------------------------------------------------

--
-- Table structure for table `blogger`
--

CREATE TABLE `blogger` (
  `nib` varchar(15) NOT NULL,
  `nama` varchar(90) NOT NULL,
  `pass` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogger`
--

INSERT INTO `blogger` (`nib`, `nama`, `pass`) VALUES
('121085', 'Andreww', 'd8578edf8458ce06fbc5bb76a58c5ca4'),
('121086', 'Adji', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Table structure for table `Comment`
--

CREATE TABLE `Comment` (
  `id` int(11) NOT NULL,
  `nama` varchar(90) NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `message` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Comment`
--

INSERT INTO `Comment` (`id`, `nama`, `email`, `no_telp`, `message`) VALUES
(3, 'Adji', 'adjiiniadji@gmail.com', '085784089212', 'Kalo bisa blog nya dimasukin gambar biar lebih cantik gan!!!'),
(4, 'Ferdy', 'ferdypinter@gmail.com', '081325928453', 'Blog nya terlalu ambigu'),
(5, 'Someone', 'someone@gmail.com', '08179379175', 'Good Job Bro!!!'),
(6, 'Adji', 'adji@gmail.com', '08179379175', 'Good Blog gan');

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `nip` varchar(15) NOT NULL,
  `nama` varchar(90) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`nip`, `nama`, `pass`, `level`) VALUES
('100001', 'Leader', 'e10adc3949ba59abbe56e057f20f883e', 1),
('100002', 'Sub Leader', 'd8578edf8458ce06fbc5bb76a58c5ca4', 2),
('100003', 'C_Chan', 'cc318a2505ea2de8419365de533f1d9e', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(1, 'C_Chan', 'iniadmin_123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogger`
--
ALTER TABLE `blogger`
  ADD PRIMARY KEY (`nib`);

--
-- Indexes for table `Comment`
--
ALTER TABLE `Comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `Comment`
--
ALTER TABLE `Comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
