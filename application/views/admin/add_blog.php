<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Add Blog</h1>
			<form class="form-horizontal" action="<?php echo base_url(). 'homeadmin/tambah' ?>" method = "post">
				<div class="form-group">
					<label class="control-label col-sm-2" for="Judul">Judul : </label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="judul" placeholder="Enter Judul" required
						name="judul" />
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="isi">Deskripsi : </label>
					<div class="col-sm-10">
						<textarea  id="isi" cols="50" rows="10" placeholder="Deskripsi disini!!" required
						name="deskripsi"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="creator">Penulis : </label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="creator" placeholder="Penulis" required
						name="penulis" />
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="tanggal">Tanggal : </label>
					<div class="col-sm-10">
						<input type="date" class="form-control" id="tanggal" required
						name="tanggal" />
					</div>
				</div>
				<center>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-success"><i class="fa fa-plus-square fa-fw"></i> Add</button>
						</div>
					</div>
				</center>
			</form> 
		</div>
	</div>
</div>