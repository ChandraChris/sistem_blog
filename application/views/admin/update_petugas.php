<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Add Petugas</h1>
			<?php foreach ($petugas as $row): ?>	
				<form class="form-horizontal" action="<?= base_url('homeadmin/process_update_petugas');?>"
					method = "POST">
					<div class="form-group">
						<label class="control-label col-sm-2" for="nip">Nip : </label>
						<div class="col-sm-10">
							<input readonly type="text" class="form-control" id="nip" placeholder="Enter Nip"
							name="nip" value="<?= $row->nip ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="username">Username : </label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="username" placeholder="Enter Username" required
							name="username" value="<?= $row->nama ?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="level">Level : </label>
						<div class="col-sm-10">
							<input type="number" class="form-control" id="level" placeholder="Enter Level" required
							name="level" value="<?= $row->level ?>" />
						</div>
					</div>
					<center>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-success"><i class="fa fa-plus-square fa-fw"></i> Edit</button>
							</div>
						</div>
					</center>
				</form>
			<?php endforeach ?>
		</div>
	</div>
</div>