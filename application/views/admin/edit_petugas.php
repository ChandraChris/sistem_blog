<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Edit Petugas</h1>
			<div class="table-responsive">
				<table class="table table-hover table-bordered table-condensed">
					<thead>
						<tr>
							<th>Nip</th>
							<th>Nama</th>
							<th>Level</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($petugas->result() as $key): ?>
							<tr>
								<td><?php echo $key->nip; ?></td>
								<td><?php echo $key->nama; ?></td>
								<td><?php echo $key->level; ?></td>
								<td>
									<center>
										<a href="<?= base_url('homeadmin/update_petugas/' . $key->nip) ?>" class="btn btn-info">Edit</a> || <a href="<?= base_url('homeadmin/delete_petugas/' . $key->nip) ?>" class="btn btn-danger">Delete
										</a>
									</center>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			<center>
				<a href="<?= base_url('homeadmin/add_petugas') ?>" class="btn btn-success">Add</a>
			</center>
		</div>
	</div>
</div>