<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|,F&X]VFWk51%)[ovn/rG=Ph&[}3 TB!mwv5<HK&23~Mlm|P}B{;xhv(pXClg@b<');
define('SECURE_AUTH_KEY',  ')Z.%}K$N[fU`PA({->q0_[f0g;M8:eY22/6SuAk^Z9{cu !NFw(0%[jvi;I+0Z;a');
define('LOGGED_IN_KEY',    'OHJM-xhUHDCi#dfSI|e/[@|;O$U/<i)_ZCvd(>dCR9*N2zsAk!1wH43SAOPvj!]T');
define('NONCE_KEY',        'SDUy!dG!FAvLqp=jIt3MeA=xFbO5b@X@]bH]a]oT-eX3(LQ<Kb.YkJm{v[,TSOt6');
define('AUTH_SALT',        '#usx=A7rgJF&B% 4cV=xvS2+n3CD7v7l4WJ:46N&Y7T5MM1H3Bd=@`laT ley0LO');
define('SECURE_AUTH_SALT', 'o<H$V#:k.tjdk+af`F]2wTBCJRo$SGU76Bps!wg1cPLjXSR|eWLh(_[~?,SDrvFA');
define('LOGGED_IN_SALT',   ']C}d5C 8sDXgK3sf?,GC-h2>dW.qW &#/![SalLJHBw<jku^S }qZ5`Ne5!]SVaZ');
define('NONCE_SALT',       'zXTsCf|rA}G0K*a[W`e9;eX5$mso},RStHI@Qzf+gmq~7^[CADhMx`,$ZI03DoeZ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
