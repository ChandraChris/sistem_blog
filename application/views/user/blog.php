<div class="container">
  <div class="row">
    <div class="col-lg-8 col-md-10 mx-auto">
      
      <hr>
      <?php foreach ($blog->result() as $key): ?>
      <div class="post-preview">
          <h2 class="post-title">
            <?php echo $key->judul;?>
          </h2>
          <p class="post-subtitle">
            <?php 
            echo $key->isi;
            ?>
          </p>
        <p class="post-meta">Posted by
          <a href="#"><?php echo $key->penulis;?></a>
        on <?php echo $key->tanggal?></p>
      </div>
      <?php endforeach ?>
      <!-- Pager -->
      <div class="clearfix">
        <?php foreach ($ninpo->result() as $key ): ?>
          <a class="btn btn-success float-right" href="<?= base_url('home/readmore/' . $key->id) ?>">Next Posts&rarr;</a>
        <?php endforeach ?>
        <?php foreach ($katon->result() as $key): ?>
          <a class="btn btn-info float-left" href="<?= base_url('home/readmore/' . $key->id) ?>">&larr;Back Posts</a>
        <?php endforeach ?>
        <center>
          <a class="btn btn-warning" href="<?= base_url() . 'home'?>">Home</a>
        </center>
      </div>
    </div>
  </div>
</div>