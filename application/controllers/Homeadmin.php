<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homeadmin extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->model('M_blog');
		$this->load->model('M_data');
		$this->load->model('M_login');
		$this->load->model('M_petugas');
		if ($this->session->userdata('masuk') != TRUE) {
			$url = base_url();
			redirect($url,'refresh');
		}
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->template->load('admin/index', 'admin/home');
	}

	public function all_blog(){
		if ($this->session->userdata('akses')=='1' || $this->session->userdata('akses') == '2' || $this->session->userdata('akses') == '3') {
			$data['blog'] = $this->M_blog->GetBlog();
			$this->template->load('admin/index', 'admin/all_blog', $data);
		} else {
			echo 'Anda tidak berhak mengakses halaman ini';
		}
	}
	public function add_blog(){
		if ($this->session->userdata('akses')=='1' || $this->session->userdata('akses') == '2') {
			$this->template->load('admin/index', 'admin/add_blog');	
		} else {
			$this->template->load('admin/index', 'admin/no_hak');
		}
	}

	public function tambah(){
		if ($this->session->userdata('akses')=='1' || $this->session->userdata('akses') == '2') {
			$judul = $this->input->post('judul');
			$deskripsi = $this->input->post('deskripsi');
			$penulis = $this->input->post('penulis');
			$tanggal = $this->input->post('tanggal');

			$data = array(
				'id' => '',
				'judul' => $judul,
				'isi' => $deskripsi,
				'penulis' => $penulis,
				'tanggal' => $tanggal);

			$this->M_data->input_data($data, 'blog');
			redirect('home/index','refresh');	
		} else {
			$this->template->load('admin/index', 'admin/no_hak');
		}
	}

	public function all_petugas(){
		if ($this->session->userdata('akses') == '1') {
			$data['petugas'] = $this->M_petugas->GetPetugas();
			$this->template->load('admin/index', 'admin/all_petugas', $data);
		} else {
			$this->template->load('admin/index', 'admin/no_hak');
		}
	}

	public function edit_petugas(){
		if ($this->session->userdata('akses') == '1') {
			$data['petugas'] = $this->M_petugas->GetPetugas();
			$this->template->load('admin/index', 'admin/edit_petugas', $data);
		} else {
			$this->template->load('admin/index', 'admin/no_hak');
		}
	}
	public function add_petugas(){
		if ($this->session->userdata('akses') == '1') {
			$this->template->load('admin/index', 'admin/add_petugas');
		} else {
			$this->template->load('admin/index', 'admin/no_hak');
		}
	}
	public function update_petugas($nip){
		$where = array(
			'nip' => $nip
		);
		$data['petugas'] = $this->M_data->edit_data($where, 'petugas')->result();
		$this->template->load('admin/index', 'admin/update_petugas', $data);
	}
	function delete_petugas($nip){
		$where = array('nip' => $nip);
		$this->M_data->hapus_data($where, 'petugas');
		redirect(base_url('homeadmin/all_petugas'),'refresh');
	}
	public function process_add_petugas(){
		$nip = $this->input->post('nip');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$passwordMD5 = md5($password);
		$level = $this->input->post('level');

		$data = array(
			'nip' => $nip,
			'nama' => $username,
			'pass' => $passwordMD5,
			'level' => $level
		);
		$this->M_data->input_data($data, 'petugas');
		redirect(base_url('homeadmin/all_petugas'),'refresh');
	}
	public function process_update_petugas(){
		$nip = $this->input->post('nip');
		$username = $this->input->post('username');
		$level = $this->input->post('level');

		$data = array( 
			'nama' => $username,
			'level' => $level
		);
		$where = array(
			'nip' => $nip
		);
		$this->M_data->update_data($where, $data, 'petugas');
		redirect('homeadmin/all_petugas','refresh');

	}

	public function all_comment(){
		if ($this->session->userdata('akses') == '1' || $this->session->userdata('akses') == '2') {
			$data['komen'] = $this->M_blog->GetComment();
			$this->template->load('admin/index', 'admin/all_comment', $data);
		} else {
			$this->template->load('admin/index', 'admin/no_hak');
		}
	}
}
