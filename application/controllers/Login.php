<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->model('M_login');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if ($this->session->userdata('masuk') == TRUE) {
			redirect(base_url('homeadmin'),'refresh');
			echo "<script>
					alert('Selamat datang');
				</script>";
		} else {
			$this->load->view('login');	
		}
	}

	// public function do_login(){
	// 	$username = $this->input->post('username');
	// 	$password = $this->input->post('password');
	// 	$alert = '<div class="alert alert-danger alert-dismissible">
	// 				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	// 				<strong>Alert!!!</strong>
	// 				<span>Username atau password yang anda masukkan salah</span>
	//  			</div>';

	// 	$cek = $this->M_login->cek_user($username, $password);
	// 	if (count($cek) == 1) {
	// 		$this->session->set_userdata(array(
	// 			'isLogin' => TRUE,
	// 			'username' => $username
	// 		));
	// 		redirect( base_url('homeadmin'),'refresh');
	// 	} else {
	// 		$this->session->set_flashdata('alert', $alert);
	// 		redirect(base_url('login'),'refresh');
	// 	}
	// }

	function auth(){
		$username = $this->input->post('username');
		$password = htmlspecialchars($this->input->post('password', TRUE), ENT_QUOTES);

		$cek_petugas = $this->M_login->auth_petugas($username, $password);

		if ($cek_petugas->num_rows() > 0) { /*Jika login sebagai petugas*/
			$data = $cek_petugas->row_array();
			$this->session->set_userdata('masuk', TRUE);
			if ($data['level']=='1') { /*Akses Admin */
				$this->session->set_userdata('akses', '1');
				$this->session->set_userdata('ses_id', $data['nip']);
				$this->session->set_userdata('ses_nama', $data['nama']);
				redirect('homeadmin','refresh');
			} else {/*Akses Petugas*/
				$this->session->set_userdata('akses', '2');
				$this->session->set_userdata('ses_id', $data['nip']);
				$this->session->set_userdata('ses_nama', $data['nama']);
				redirect('homeadmin','refresh');
			}
		} else {/*Jika login sebagai Mahasiswa*/
			$cek_blogger = $this->M_login->auth_blogger($username, $password);
			if ($cek_blogger->num_rows() > 0) {
				$data = $cek_blogger->row_array();
				$this->session->set_userdata('masuk', TRUE);
				$this->session->set_userdata('akses', '3');
				$this->session->set_userdata('ses_id', $data['nib']);
				$this->session->set_userdata('ses_nama', $data['nama']);
				redirect('homeadmin','refresh');
			} else {/*Jika username dan password tidak ditemukan*/
				$url = base_url('login');
				echo $this->session->set_flashdata('msg', 'username atau password salah');
				redirect($url,'refresh');
			}
		}
	}
	function logout(){
		$this->session->sess_destroy();
		$url = base_url();
		redirect($url,'refresh');
	}
}
