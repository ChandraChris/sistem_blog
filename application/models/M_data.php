<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_data extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function input_data($data, $table){
		$data = $this->db->insert($table, $data);
		return $data;
	}

	function edit_data($where, $table){
		return $this->db->get_where($table, $where);
	}
	function update_data($where, $data, $table){
		$this->db->where($where);
		return $this->db->update($table, $data);
	}
	function hapus_data($where, $table){
		$this->db->where($where);
		$this->db->delete($table);
	}
}
